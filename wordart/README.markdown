# WordArt

A Pen created on CodePen.io. Original URL: [https://codepen.io/kathykato/pen/omxPap](https://codepen.io/kathykato/pen/omxPap).

Recreated some WordArt in pure CSS! You can type your own text and the text style changes based on the style you choose. Looks best on desktop.
